<?php
$code='<?php

$hello = "Hello, World !!";

echo "<p>{$hello}</p>";

$hell = str_replace("o","!",$hello);

echo "<p>{$hell}</p>";

?>';
$filename="demo.php";
$filepath="./upload/";

if(isset($_POST['cmd'])){
  if($_POST['cmd']=='run' && isset($_POST['code']) &&isset($_POST['filename'])){
    $code = $_POST['code'];
    $filename = basename($_POST['filename']);
    $fp = fopen($filepath . $filename, "w");  
    fwrite($fp, $code);  
    fclose($fp);  
  } elseif ($_POST['cmd']=='load' && isset($_POST['filename'])){
    $filename = $_POST['filename'];
    if(file_exists($filepath . $filename)){
      $fp = fopen($filepath . $filename, "r");
      $code = fread($fp, filesize($filepath . $filename));
      fclose($fp); 
    } else {
      $code = "File is Not Exist !";
    }
  }
}



$code = rawurlencode($code);

?>


<!DOCTYPE html>
<html>
<head>
  <title>PHP Tester</title>
  <link rel="stylesheet" type="text/css" media="screen" href="http://ionicons.com/css/ionicons.min.css?v=2.0.1">
</head>
<body>
  <div id="main">
  <div class="nav">
        <form id="runform" method="POST">
        <ul>
          <li><i style="margin-left:10px;margin-right:-90px;">Filename:</i><input id="filename" style="padding-left:82px" type="text" name="filename" placeholder="filename" value="<?=$filename?>"></li>
          <li><i style="margin-left:9px;margin-right:-20px;" class="ion-play"></i><input type="button" value="Run" onclick="run()"></li>
          <li><i style="margin-left:6px;margin-right:-25px;" class="ion-cloud"></i><input type="button" value="Load" onclick="load()"></li>
          <li><input type="file" id="import-file" value="Import"><i style="margin-left: -175px;margin-right: 37px;float: right;z-index:-10">Import From Local</i></li>
          <li><input id="code" name="code" type="hidden" value="<?=$code?>"></li>
          <li><input id="cmd" name="cmd" type="hidden" value=""></li>
        </ul>
        </form>
      </div>
    <div class="wrapper">
      <div id="editor"></div>
    </div>
    <hr class="vertical"/>
    <div class="wrapper">
      <iframe id="ifrm" src="<?=$filepath.$filename?>" frameborder=0 width="100%"  scrolling=no border=0 ></iframe>
    </div>
  </div>
<style type="text/css">
html, body, iframe {width: 100%; height: 100%; overflow:hidden;}
iframe {
  margin:8px 8px 8px 8px;
  height: 100%;
}
hr.vertical{
  width: 0px;
  height: 100%;
  border:none;
  border-left: 2px #5A9 solid;
  padding: 0;
  margin:0px -10px 0px 3px;
  margin-left: 3px;
  margin-right: -10px;
  display: block;
  float: left;
}

i {
  padding: 0.2em;
  font-family: Monaco, Menlo, 'Ubuntu Mono', Consolas, source-code-pro, monospace;
  font-style: normal;
  font-size: 13px;
}
#main{
  display: block;
  width: 100%;
  height: 90%;
}
.wrapper{
  display: inline-block;
  width: 50%;height: 100%;
  float: left;
  overflow: hidden;
}

#editor{
  position: relative;
  width: 100%;
  height: 100%;
}
#result iframe{
  width: 100%;
  height: 100%;
}

.nav {
  height: 45px;
  width: 100%;
  box-shadow: 1px 1px #bbb;
  margin-top: 0px;
  margin-bottom: 4px;
}

.nav form ul {
  list-style: none;
  margin: 0px;
  padding: 0px;
}

.nav form ul li {
  display: inline-block;
}

.nav form ul li>i{
  position: relative;
  top: 7px;
  float: left;
}

.nav form input {
  border: none;
  border-bottom: solid 3px #FFF;
  min-width: 70px;
  background-color: transparent;
  float: left;
}

.nav form input:hover {
  border-bottom: solid 3px #1C90F3;
}

.nav form input[type=text]{
  padding: 10px 15px 10px 30px;
}
.nav form input[type=text]:focus{
  background-color: #ffedc0;
}
.nav form input[type=button]{
  padding: 10px 15px 10px 20px;
  background-color: #FFF;
}
.nav form input[type=button]:active{
  background-color: #ffd200;
  color: #000;
  font-weight: 900;
}
.nav form input[type=file]{
  padding: 10px 15px 10px 20px;
  padding: 7.5px 15px;
  color: white;
  background-color: white;
}
.nav form input[type=file]:hover{
  color: transparent;
  background-color: transparent;
}
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.2.3/ace.js" type="text/javascript" charset="utf-8"></script>
<script>

var editor = ace.edit("editor");

function run(){
  document.getElementById("code").value = editor.getValue();
  document.getElementById("cmd").value = "run";
  document.getElementById("runform").submit();
}

function load(){
  document.getElementById("cmd").value = "load";
  document.getElementById("runform").submit();
}
function _import(){
  document.getElementById("cmd").value = "import";
  document.getElementById("runform").submit();
}

// iframe resize
function iframe_init(){
  var obj = document.getElementById("ifrm"); 
  var ch = obj.contentWindow.document.body.scrollHeight; 
  obj.style.height = ch-20; 
}


window.onload = function () { 
  editor.setTheme("ace/theme/monokai");
  editor.getSession().setMode("ace/mode/php");
  editor.setValue(decodeURIComponent(document.getElementById("code").value));
 //Check the support for the File API support 
 if (window.File && window.FileReader && window.FileList && window.Blob) {
    var fileSelected = document.getElementById('import-file');
    var filename = document.getElementById('filename');
    fileSelected.addEventListener('change', function (e) { 
         //Set the extension for the file 
         var fileExtension = /text.*/; 
         //Get the file object 
         var fileTobeRead = fileSelected.files[0];
        //Check of the extension match 
         if (fileTobeRead.type.match(fileExtension)) { 
             //Initialize the FileReader object to read the 2file 
             var fileReader = new FileReader(); 
             fileReader.onload = function (e) { 
                 editor.setValue(fileReader.result);
             } 
             fileReader.readAsText(fileTobeRead); 
             document.getElementById("filename").value = fileSelected.files[0].name;
         } 
         else { 
             alert("Please select text file"); 
         }
 
    }, false);
} 
 else { 
     alert("Files are not supported"); 
 } 
}
</script>
</body>
</html>